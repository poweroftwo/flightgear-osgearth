//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifdef ENABLE_OSGEARTH

#include <osg/TextureRectangle>
#include <osg/Texture2D>
#include <osg/ComputeBoundsVisitor>
#include <osgDB/WriteFile>
#include <osgEarth/VirtualProgram>
#include <osgEarth/ElevationQuery>
#include <Airports/airport.hxx>
#include <Airports/runwaybase.hxx>
#include <Airports/runways.hxx>
#include <osgViewer/Viewer>
#include "renderer.hxx"
#include <osgText/Text>
#include <Main/globals.hxx>
#include <Main/fg_props.hxx>
#include <simgear/scene/util/OsgMath.hxx>
#include <boost/format.hpp>
#include <osgDB/ReadFile>
#include <osg/MatrixTransform>
#include <GL/glu.h>
#include <osgDB/FileNameUtils>

#include <simgear/scene/util/OsgEarthHeightCache.hxx>
#include <Scenery/scenery.hxx>
#include <boost/algorithm/string.hpp>    



#if defined(_WIN32) || defined(WIN32) 
#   include <cpl_string.h>
#   include <gdal_priv.h>
#   include <cpl_conv.h>
#   include <ogr_spatialref.h>
#else
#   include <gdal/cpl_string.h>
#   include <gdal/gdal_priv.h>
#   include <gdal/cpl_conv.h>
#   include <gdal/ogr_spatialref.h>
#endif

#ifdef ENABLE_OSGEARTH
#   include <simgear/scene/util/OsgEarthHeightCache.hxx>
#endif

#include "OsgEarthHeightField.hxx"
#include <osgEarthAnnotation/ModelNode>


// globals
const double g_ZNear = 1.0;
const double g_ZFar = 4000.0;


// transformation matrix from ENU (simulation frame) to EUS (OpenGL frame)
static const osg::Matrixd s_Enu2eus = osg::Matrixd::rotate(
    osg::PI / 2.0, osg::Vec3d(1.0, 0.0, 0.0));

osg::Camera *createRttCamera(const unsigned int targetWidth,
                             const unsigned int targetHeight)
{
    osg::Camera* camera = new osg::Camera;

    // set clear color to ensure alpha is 0 by default to mark as NoData
    camera->setClearColor(osg::Vec4(0.0, 0.0, 0.0, 0.0));
    camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    camera->setRenderOrder(osg::Camera::PRE_RENDER);
    camera->setRenderTargetImplementation(osg::Camera::FRAME_BUFFER_OBJECT);

    camera->setViewport(0, 0, targetWidth, targetHeight);

    // set up a stateset for the RTT camera
    osg::StateSet* rttStateSet = camera->getOrCreateStateSet();
    rttStateSet->setMode(GL_CULL_FACE,osg::StateAttribute::OFF);
    rttStateSet->setMode(GL_LIGHTING,osg::StateAttribute::OFF);

    return camera;
}

bool SaveGeoTiff(const char* filename,
                 osg::Camera* rttCamera,
                 osg::Image* heightImage,
                 const SGGeod northWestGeod, 
                 const double xScaleDeg, 
                 const double yScaleDeg, 
                 const double xSkewDeg, 
                 const double ySkewDeg) 
{
    bool ret = false;

    GDALAllRegister();

    GDALDriver* poDriver = GetGDALDriverManager()->GetDriverByName("GTiff");

    if (poDriver != NULL) {

        char **papszOptions = NULL;

        const unsigned int imgW = heightImage->s();
        const unsigned int imgH = heightImage->t();

        GDALDataset *poDstDS = poDriver->Create(
            filename, imgW, imgH, 1, GDT_Float32, papszOptions);

        if (poDstDS != NULL) {
            SG_LOG(SG_TERRAIN, SG_INFO, 
                "[OsgEarthHeightField] creating GeoTiff: " << filename << std::endl);
        }

        /*  
            gdal API tutorial:
                http://www.gdal.org/gdal_tutorial.html

            A - top left longitude (degrees), 
            B - w-e pixel resolution (degrees), 
            C - rotation, 
            D - top left latitude (degrees),
            E - rotation, 
            F - n-s pixel resolution (degrees) 

            GeoTiff geo reference definition
            http://www.remotesensing.org/geotiff/faq.html

            Xgeo = A + Xpix*B + Ypix*C
            Ygeo = D + Xpix*E + Ypix*F

            good ESRI world file header description
            http://en.wikipedia.org/wiki/World_file

            description of the geotiff coordinate transformations
            http://www.remotesensing.org/geotiff/spec/geotiff2.6.html
        */
        double adfGeoTransform[6] = 
        {
            northWestGeod.getLongitudeDeg(), 
            xScaleDeg,
            xSkewDeg, 
            northWestGeod.getLatitudeDeg(),
            ySkewDeg,
            yScaleDeg
        };

        OGRSpatialReference oSRS;
        char *pszSRS_WKT = NULL;

        unsigned int imageSize = imgW * imgH;
        float *abyRaster = new float[imageSize];

        const double noDataValue = -9999.0;

        double l, r, b, t, nearClip, farClip;
        rttCamera->getProjectionMatrixAsOrtho(l, r, b, t, nearClip, farClip);

        // --- Float data ---
        for (unsigned int rowi = 0; rowi <imgH; ++rowi) {
            for (unsigned int coli = 0; coli < imgW; ++coli) {

                // raw depth 
                float depth = *(float*)heightImage->data(coli, rowi);

                // note: depth buffer is linear for orthographic projections
                float height = ((farClip - nearClip)/2) * (depth*2) + nearClip;

                // index into GeoTiff array
                unsigned int index = ((imgH - (((imgH-1)-rowi)+1))*imgW)+coli;

                if (height == 0) {
                    abyRaster[index] = noDataValue;
                } else {
                    // store height (meters)
                    abyRaster[index] = height;
                }
            }
        }

  

        // set geographic encoding
        poDstDS->SetGeoTransform(adfGeoTransform);

        oSRS.SetWellKnownGeogCS("WGS84");
        oSRS.exportToWkt(&pszSRS_WKT);
        poDstDS->SetProjection(pszSRS_WKT);
        CPLFree(pszSRS_WKT);

        GDALRasterBand *poBand = poDstDS->GetRasterBand(1);
        GDALSetRasterNoDataValue(poBand, noDataValue);

        CPLErr rasterError = poBand->RasterIO(GF_Write, 0, 0, imgW, imgH, 
            abyRaster, imgW, imgH, GDT_Float32, 0, 0);    

        ret = (rasterError == CE_None) ? true : false;

        // close GDAL
        GDALClose((GDALDatasetH) poDstDS);

        delete[] abyRaster;
    }

    return ret;
}

bool RenderSceneToGeoTiff(const char* name,
                          osg::Group* sceneRoot,
                          const SGGeod minGeodetic,
                          const SGGeod maxGeodetic)
{
    bool ret = false;

    // --- create an RTT camera to render the GeoTiff(s) ---

    osgViewer::Viewer* viewer = globals->get_renderer()->getViewer();

    if (viewer->getSceneData() != NULL) {
        // target image resolution
        unsigned texSize = 16;
        osg::ref_ptr<osg::Camera> rttCam = createRttCamera(texSize, texSize);

        // --- create capture image ----
        osg::Image* fboImage = new osg::Image();
        fboImage->setName("capture");
        fboImage->allocateImage((int)texSize, (int)texSize, 1, GL_DEPTH_COMPONENT, GL_FLOAT);
        
        rttCam->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        rttCam->setClearDepth(1.0); 
        
        rttCam->attach(osg::Camera::DEPTH_BUFFER, fboImage);

        osgViewer::Viewer::Contexts gfxContextList;
        viewer->getContexts(gfxContextList);

        if (gfxContextList.size() > 0) {
            // re-use graphics context
            rttCam->setGraphicsContext(gfxContextList[0]);
        }

        // --- point RTT camera to look at extents of graph
        osg::Matrixd mat = osg::Matrixd::identity();

        // position camera at center of the bounding box, 
        // stare from earth's origin, 
        // camera up should be tangential to earth pointing north

        SGGeod centerLatLon = SGGeod::fromRad(
            (minGeodetic.getLongitudeRad() + maxGeodetic.getLongitudeRad()) / 2.0,
            (minGeodetic.getLatitudeRad() + maxGeodetic.getLatitudeRad()) / 2.0);
        SGVec3d center = SGVec3d::fromGeod(centerLatLon);

        // retrieve local frame
        osg::Matrixd localFrame = 
            simgear::OsgEarthHeightCache::Instance()->GetCoordinateSystemNode()->
                computeLocalCoordinateFrame(toOsg(center));

        // stare upward
        mat = osg::Matrixd::rotate(osg::PI/2.0, osg::X_AXIS) * localFrame;
        mat.setTrans(toOsg(center));

        // transform from ENU to EUS and invert to get OpenGL view matrix
        osg::Matrixd viewMat = osg::Matrixd::inverse(s_Enu2eus * mat);
        rttCam->setViewMatrix(viewMat);

        // set orthographic project matrix to encompass extents

        // geodetic extents
        SGGeod northWestGeod = SGGeod::fromRad(
            minGeodetic.getLongitudeRad(), 
            maxGeodetic.getLatitudeRad());
        SGGeod northEastGeod = SGGeod::fromRad(
            maxGeodetic.getLongitudeRad(), 
            maxGeodetic.getLatitudeRad());
        SGGeod southEastGeod = SGGeod::fromRad(
            maxGeodetic.getLongitudeRad(),
            minGeodetic.getLatitudeRad());
        SGGeod southWestGeod = SGGeod::fromRad(
            minGeodetic.getLongitudeRad(), 
            minGeodetic.getLatitudeRad());

        SGVec3d northWestGeoc = SGVec3d::fromGeod(northWestGeod);
        SGVec3d northEastGeoc = SGVec3d::fromGeod(northEastGeod);
        SGVec3d southEastGeoc = SGVec3d::fromGeod(southEastGeod);
        SGVec3d southWestGeoc = SGVec3d::fromGeod(southWestGeod);

        // width and height in meters
        double widthMeters = length(northWestGeoc - northEastGeoc);
        double heightMeters = length(northWestGeoc - southWestGeoc);
        
        // orthographic project matrix (left , right, bottom, top, zNear, zFar)
        rttCam->setProjectionMatrix(
            osg::Matrixd::ortho(-widthMeters/2.0, widthMeters/2.0, 
                                -heightMeters/2.0, heightMeters/2.0,
                                g_ZNear, g_ZFar));

        // --- create a custom osgUtil::SceneView ---
        osgUtil::SceneView* sv = new osgUtil::SceneView();
        sv->setDefaults();

        osgUtil::CullVisitor *customCullVisitor = new osgUtil::CullVisitor();
        customCullVisitor->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);
        sv->setCullVisitor(customCullVisitor);

        sv->setCamera(rttCam);

        // point RTT camera to point at the rendered scene
        rttCam->addChild(sceneRoot);

        // build render graph
        sv->cull();
        // draw the scene to texture
        sv->draw();

        // clean up scene graph
        rttCam->removeChild(sceneRoot);

        // horizontal resolution (pixels / degree)
        double horizontalResDeg = 
            fabs(northWestGeod.getLongitudeDeg() - northEastGeod.getLongitudeDeg()) /
            fboImage->s();

        // vertical resolution (pixels / degree)
        double verticalResDeg = 
            fabs(northWestGeod.getLatitudeDeg() - southWestGeod.getLatitudeDeg()) / 
            fboImage->t();

        // save a geographically referenced TIFF from the captured image
        ret = SaveGeoTiff(
                name, rttCam, fboImage, northWestGeod, 
                horizontalResDeg, -verticalResDeg, 
                0.0f, 0.0f);
    }

    return ret;
}


osg::Geode* buildAirportScene(const osg::BoundingBoxd sampleLonLatRadBound,
                              const osg::BoundingBoxd geoLonLatRadBound)
{
    // --- build entire airport extents as a single geode ---

    // --- sample heights at corners of extents for polygon coloring ---

    osg::Vec3d upV;
    const static double minHeight = 10.0;

    // sw
    double swHeight = 0.0;
    SGGeod swGeod = SGGeod::fromRad(sampleLonLatRadBound.xMin(), sampleLonLatRadBound.yMin());
    simgear::OsgEarthHeightCache::Instance()->SampleHeight(swGeod, swHeight, upV);   
    swHeight = osg::maximum(g_ZNear+minHeight, swHeight);
    SG_LOG(SG_TERRAIN, SG_INFO, "    sw corner height: " << swHeight << std::endl);
    // use scaled size for geometric extents
    SGVec3d sw = SGVec3d::fromGeod(SGGeod::fromRadM(geoLonLatRadBound.xMin(), geoLonLatRadBound.yMin(), swHeight));

    // se
    double seHeight = 0.0;
    SGGeod seGeod = SGGeod::fromRad(sampleLonLatRadBound.xMax(), sampleLonLatRadBound.yMin());
    simgear::OsgEarthHeightCache::Instance()->SampleHeight(seGeod, seHeight, upV);
    seHeight = osg::maximum(g_ZNear+minHeight, seHeight);
    SG_LOG(SG_TERRAIN, SG_INFO, "    se corner height: " << seHeight << std::endl);
    // use scaled size for geometric extents
    SGVec3d se = SGVec3d::fromGeod(SGGeod::fromRadM(geoLonLatRadBound.xMax(), geoLonLatRadBound.yMin(), seHeight));

    // ne
    double neHeight = 0.0;
    SGGeod neGeod = SGGeod::fromRad(sampleLonLatRadBound.xMax(), sampleLonLatRadBound.yMax());
    simgear::OsgEarthHeightCache::Instance()->SampleHeight(neGeod, neHeight, upV);
    neHeight = osg::maximum(g_ZNear+minHeight, neHeight);
    SG_LOG(SG_TERRAIN, SG_INFO, "    ne corner height: " << neHeight << std::endl);
    // use scaled size for geometric extents
    SGVec3d ne = SGVec3d::fromGeod(SGGeod::fromRadM(geoLonLatRadBound.xMax(), geoLonLatRadBound.yMax(), neHeight));

    // nw
    double nwHeight = 0.0;
    SGGeod nwGeod = SGGeod::fromRad(sampleLonLatRadBound.xMin(), sampleLonLatRadBound.yMax());
    simgear::OsgEarthHeightCache::Instance()->SampleHeight(nwGeod, nwHeight, upV);
    nwHeight = osg::maximum(g_ZNear+minHeight, nwHeight);
    SG_LOG(SG_TERRAIN, SG_INFO, "    nw corner height: " << nwHeight << std::endl);
    // use scaled size for geometric extents
    SGVec3d nw = SGVec3d::fromGeod(SGGeod::fromRadM(geoLonLatRadBound.xMin(), geoLonLatRadBound.yMax(), nwHeight));

    // center 
    double lat = sampleLonLatRadBound.yMin() + (sampleLonLatRadBound.yMax() - sampleLonLatRadBound.yMin()) / 2.0;
    double lon = sampleLonLatRadBound.xMin() + (sampleLonLatRadBound.xMax() - sampleLonLatRadBound.xMin()) / 2.0;
    double centerHeight = 0.0;
    SGGeod centerGeod = SGGeod::fromRad(lon, lat);
    simgear::OsgEarthHeightCache::Instance()->SampleHeight(centerGeod, centerHeight, upV);

    // ensure that the center point is at least below the 
    // plane defined by three of the corners
    float interpolateHeight = neHeight + (seHeight - neHeight)/2.0;

    if (centerHeight > interpolateHeight) {
        centerHeight = interpolateHeight;
    }

    centerHeight = osg::maximum(g_ZNear+minHeight, centerHeight);
    SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthHeightField] center height: " << centerHeight);
    SGVec3d center = SGVec3d::fromGeod(SGGeod::fromRadM(lon, lat, centerHeight));

    osg::ref_ptr<osg::Geometry> triStrip = new osg::Geometry;

    // Create an array of four vertices to make up two triangles
    osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array;
    triStrip->setVertexArray(v.get());

    v->push_back(toOsg(nw));
    v->push_back(toOsg(sw));
    v->push_back(toOsg(center));

    v->push_back(toOsg(ne));
    v->push_back(toOsg(nw));
    v->push_back(toOsg(center));

    v->push_back(toOsg(se));
    v->push_back(toOsg(ne));
    v->push_back(toOsg(center));

    v->push_back(toOsg(sw));
    v->push_back(toOsg(se));
    v->push_back(toOsg(center));


    osg::StateSet* stateset = new osg::StateSet;
    triStrip->setStateSet(stateset);

    stateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    stateset->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);

    // draw as individual triangles, not as efficient, but does not matter since
    // is only rendered to RTT once
    triStrip->addPrimitiveSet(
        new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, 12));

    osg::Geode *geode = new osg::Geode;
    geode->addDrawable(triStrip.get());

    return geode;
}

void OsgEarthHeightField::MonitorKMLModelClamping()
{
    // --- reclamp KML models whenever new height fields are built ---

    if (simgear::OsgEarthHeightCache::Instance()->IsModelClampingDirty()) {

        const float waitSec = 10.0;

        if (osg::Timer::instance()->delta_s(
            simgear::OsgEarthHeightCache::Instance()->GetModelClampingStartTime(), 
            osg::Timer::instance()->tick()) > waitSec) 
        {

            simgear::OsgEarthHeightCache::Instance()->ClearIsModelClampingDirty();

            osgEarth::FindNodesVisitor<osgEarth::Annotation::ModelNode> modelNodeFinder;

            simgear::OsgEarthHeightCache::Instance()->GetMapNode()->accept(modelNodeFinder);

            std::vector<osgEarth::Annotation::ModelNode*>::iterator i;
            for(i = modelNodeFinder._results.begin(); i != modelNodeFinder._results.end(); ++i) {
                osgEarth::Annotation::ModelNode* modelNode = *i;
                modelNode->updateTransform(modelNode->getPosition(), NULL);
            }
        }
    }
}

void OsgEarthHeightField::generateHeightField(const float rangeNm)
{
    // save path string
    std::string fullPath = simgear::OsgEarthHeightCache::Instance()->GetHeightFieldPath();
    SGPath sgPath = fullPath;

    if (sgPath.isDir() == false) {
        sgPath.add("/");
        sgPath.create_dir(0755);
        // reset path string to full path
        sgPath.set(fullPath);
    }

    // true a height field was added
    bool isAddedHeightField = false;

    double altAglFt = fgGetDouble("/position/altitude-agl-ft");

    // altitude (feet) threshold that triggers creation of an airport height field
    const double altThresholdFt = 2000.0;

    // holds current list of airport, by name, in range
    std::vector<std::string> airportsInRange;

    if (sgPath.isDir() == false) {
         SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthHeightField] Height Field Creation Error: "
               "Unable to create destination folder" << std::endl);

    } else if (altAglFt < altThresholdFt) {

        SGGeod centerPos = globals->get_aircraft_position();

        osg::ref_ptr<osg::Group> sceneRoot = new osg::Group;

        FGAirport::AirportFilter filter;
        FGPositionedList apts = FGPositioned::findWithinRange(centerPos, rangeNm, &filter);
        for (unsigned int apti = 0; apti < apts.size(); ++apti) {
            FGAirport* airport = (FGAirport*)apts[apti].get();

            std::string airportId = airport->getId();
            boost::algorithm::to_upper(airportId);

            // add airport id name to the in-range list
            airportsInRange.push_back(airportId);

            simgear::OsgEarthHeightCache *earth = simgear::OsgEarthHeightCache::Instance();

            if (earth->IsEarthReady() && 
                (earth->GetMapNode()->getMap()->getElevationLayerByName(airportId) == NULL)) 
            {
               
                // layer does not yet exist, so attempt to create height field

                SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthHeightField] adding airport: " << airportId << std::endl);
                SG_LOG(SG_TERRAIN, SG_INFO, "    num runways " << airport->numRunways() << std::endl);
                SG_LOG(SG_TERRAIN, SG_INFO, "    num taxiways " << airport->numTaxiways() << std::endl);


                // --- find airport extents ---

                std::vector<FGRunwayBase*> runwayList;

                for (unsigned int r = 0; r < airport->numRunways(); ++r) {
                    FGRunwayBase* rwy = (FGRunwayBase*)(airport->getRunwayByIndex(r));
                    runwayList.push_back(rwy);
                }

                for (unsigned int r = 0; r < airport->numTaxiways(); ++r) {
                    FGRunwayBase* rwy = (FGRunwayBase*)(airport->getTaxiwayByIndex(r));
                    runwayList.push_back(rwy);
                }

                // x = longitude, y = latitude
                osg::BoundingBoxd lonLatBound;

                for (unsigned int r = 0; r < runwayList.size(); ++r) {

                    FGRunwayBase* rwy = runwayList[r];

                    // add runway corners
                    std::vector<SGGeod> cornerList;
                    cornerList.push_back(rwy->pointOffCenterline(rwy->lengthM(), rwy->widthM()));
                    cornerList.push_back(rwy->pointOffCenterline(rwy->lengthM(), -rwy->widthM()));
                    cornerList.push_back(rwy->pointOffCenterline(0.0, rwy->widthM()));
                    cornerList.push_back(rwy->pointOffCenterline(0.0, -rwy->widthM()));

                    for (unsigned int j = 0; j < cornerList.size(); ++j) {
                        SGGeod p = cornerList[j];
                        lonLatBound.expandBy(osg::Vec3d(p.getLongitudeRad(), p.getLatitudeRad(), 0.0));
                    }
                }

                if (airport->getTowerLocation().isValid()) {
                    SGGeod towerGeod = airport->getTowerLocation();
                    lonLatBound.expandBy(osg::Vec3d(
                        towerGeod.getLongitudeRad(), 
                        towerGeod.getLatitudeRad(), 
                        0.0));
                }


                osg::BoundingBoxd sampleLonLatBound = lonLatBound;
                {
                    // --- down-scale for height sample extents ---
                    const float scale = 0.75;
                    double lonSpan = fabs(sampleLonLatBound.xMax() - sampleLonLatBound.xMin());
                    double latSpan = fabs(sampleLonLatBound.yMax() - sampleLonLatBound.yMin());
                    sampleLonLatBound.xMin() = sampleLonLatBound.xMin() - (lonSpan * ((scale - 1.0)/2.0));
                    sampleLonLatBound.xMax() = sampleLonLatBound.xMax() + (lonSpan * ((scale - 1.0)/2.0));
                    sampleLonLatBound.yMin() = sampleLonLatBound.yMin() - (latSpan * ((scale - 1.0)/2.0));
                    sampleLonLatBound.yMax() = sampleLonLatBound.yMax() + (latSpan * ((scale - 1.0)/2.0));
                }

                osg::BoundingBoxd geoLonLatBound = lonLatBound;
                {
                    // --- up-scale geometric extents ---
                    const float scale = 1.45;
                    double lonSpan = fabs(geoLonLatBound.xMax() - geoLonLatBound.xMin());
                    double latSpan = fabs(geoLonLatBound.yMax() - geoLonLatBound.yMin());
                    geoLonLatBound.xMin() = geoLonLatBound.xMin() - (lonSpan * ((scale - 1.0)/2.0));
                    geoLonLatBound.xMax() = geoLonLatBound.xMax() + (lonSpan * ((scale - 1.0)/2.0));
                    geoLonLatBound.yMin() = geoLonLatBound.yMin() - (latSpan * ((scale - 1.0)/2.0));
                    geoLonLatBound.yMax() = geoLonLatBound.yMax() + (latSpan * ((scale - 1.0)/2.0));

                }
                SG_LOG(SG_TERRAIN, SG_INFO, 
                    boost::format("[OsgEarthHeightField] airport bound: left: %f  right: %f  bottom: %f  top: %f") 
                        % osg::RadiansToDegrees(geoLonLatBound.xMin()) 
                        % osg::RadiansToDegrees(geoLonLatBound.xMax()) 
                        % osg::RadiansToDegrees(geoLonLatBound.yMin()) 
                        % osg::RadiansToDegrees(geoLonLatBound.yMax()) << std::endl);

                // --- build entire airport extents as a single geode ---
                osg::Geode* geode = buildAirportScene(sampleLonLatBound, geoLonLatBound);
            
                sceneRoot->addChild(geode);

                // build unique name based on airport
                std::string filename = airport->ident() + ".tiff";             

                SGPath path = fullPath;
                path.append(filename);



                // --- render the GeoTiff ---

                SGGeod minGeod = SGGeod::fromRadM(geoLonLatBound.xMin(), geoLonLatBound.yMin(), 0.0);
                SGGeod maxGeod = SGGeod::fromRadM(geoLonLatBound.xMax(), geoLonLatBound.yMax(), 0.0);

                bool isSave = RenderSceneToGeoTiff(path.str().c_str(), sceneRoot, minGeod, maxGeod);

                if (!isAddedHeightField && isSave) {
                    isAddedHeightField = true;
                }
            }
        }
    }

    if (isAddedHeightField) {
        // ensure models get clamped to the the earth
        simgear::OsgEarthHeightCache::Instance()->SetIsHeightFieldDirtyFlag();
        simgear::OsgEarthHeightCache::Instance()->InitEarth();
        simgear::OsgEarthHeightCache::Instance()->SetIsModelClampingDirty();

        SG_LOG(SG_TERRAIN, SG_INFO, 
            "[OsgEarthHeightField] marking KML model clamping dirty" << std::endl);

        SG_LOG(SG_TERRAIN, SG_INFO, 
            "[OsgEarthHeightField] resetting OsgEarth to add the new layer" << std::endl);
    }

    // monitor KML model clamping dirty state
    MonitorKMLModelClamping();

}

#endif