//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifndef __OsgEarthClampLightsVisitor_h__
#define __OsgEarthClampLightsVisitor_h__

#ifdef ENABLE_OSGEARTH

#include <osg/Referenced>
#include <osg/ref_ptr>
#include <osg/observer_ptr>
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osgEarth/Terrain>
#include <simgear/scene/model/SGOffsetTransform.hxx>
#include <simgear/scene/model/ModelRegistry.hxx>
#include <simgear/threads/SGGuard.hxx>

class OsgEarthClampLightsVisitor : public osg::NodeVisitor                           
{
public:
    enum ClampPhaseType {
        NotClampedPhase = 0,
        InProgressPhase,
        ReentrantPhase,
        IsClampedPhase
    };

public:
    OsgEarthClampLightsVisitor(const std::string airportName, 
                       osg::MatrixTransform* transformNode,
                       SGOffsetTransform* sgXform);

public:

    // return current clamp phase
    ClampPhaseType GetClampPhase() { return m_ClampPhase; }

    // process light clamping for a time slice
    void Process(osg::Node* subGraph = NULL);

    // return current progress as a percentage
    float GetProgressPercentage() { return ((m_ProcessedVertexCount / float(m_VertexCount))*100.0); }

    // return geodetic position
    SGGeod GetGeodPos() { return m_GeodPos; }

    // return pointer to transform node
    osg::MatrixTransform* GetTransformNode() { return m_TransformNode.get(); }

    // return pointer to the SG offset transform node
    SGOffsetTransform* GetSGOffsetTransformNode() { return m_SGTransformNode.get(); }

protected:

    virtual void apply(osg::Geode &node);

    void UpdateVertex(osg::Vec3 &v);

    // count the total number of lights that are to be clamped
    void countLights();


protected:
    // airport name as a handle
    std::string m_AirportName;

    // current processing phase
    ClampPhaseType m_ClampPhase;

    // true if transform node is clamped
    bool m_IsClampedXform;

    // transformation node's updated matrix
    osg::Matrixd m_TransformMat;

    // positions features on airport
    osg::observer_ptr<osg::MatrixTransform> m_TransformNode;
    
    // used to monitor airport's subgraph existence
    osg::observer_ptr<SGOffsetTransform> m_SGTransformNode;

    // transform node's position
    SGGeod m_GeodPos;

    // list of drawables to clamp
    std::vector<osg::ref_ptr<osg::Drawable> > m_DrawableList;
    
    // index to current drawable being processed
    unsigned int m_DrawableIndex;

    // index to vertex (of current drawable) being processed
    unsigned int m_VertexIndex;

    // number of vertices (lights) to clamp
    unsigned int m_VertexCount;

    // number of vertices that have been clamped so far
    unsigned int m_ProcessedVertexCount;
};



#endif

#endif
