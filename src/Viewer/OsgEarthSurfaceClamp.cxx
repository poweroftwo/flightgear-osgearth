//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifdef ENABLE_OSGEARTH

#include <osg/TextureRectangle>
#include <osg/Texture2D>
#include <osg/ComputeBoundsVisitor>
#include <osgDB/WriteFile>
#include <osgEarth/VirtualProgram>
#include <osgEarth/ElevationQuery>
#include <Airports/airport.hxx>
#include <Airports/runwaybase.hxx>
#include <Airports/runways.hxx>
#include <osgViewer/Viewer>
#include "renderer.hxx"
#include <osgText/Text>
#include <Main/globals.hxx>
#include <Main/fg_props.hxx>
#include <boost/format.hpp>
#include <osgDB/ReadFile>
#include <osg/MatrixTransform>
#include <GL/glu.h>
#include <osgDB/FileNameUtils>
#include <Scenery/scenery.hxx>
#include <boost/algorithm/string.hpp>    
#include <simgear/scene/util/OsgMath.hxx>
#include <simgear/scene/util/OsgEarthHeightCache.hxx>

#include "OsgEarthSurfaceClamp.hxx"

// continually clamp current object set
void OsgEarthSurfaceClamp::updateSurfaceClamping(const float rangeNm)
{

    // --- clamp lighting ---

    double altAglFt = fgGetDouble("/position/altitude-agl-ft");

    // altitude (feet) threshold that triggers creation of an airport height field
    const double altThresholdFt = 2000.0;

    // holds current list of airport, by name, in range
    std::vector<std::string> airportsInRange;
    // clear airport position associative map
    m_AirportPosMap.clear();

    if (altAglFt < altThresholdFt) {

        SGGeod centerPos = globals->get_aircraft_position();

        FGAirport::AirportFilter filter;
        FGPositionedList apts = FGPositioned::findWithinRange(centerPos, rangeNm, &filter);
        for (unsigned int apti = 0; apti < apts.size(); ++apti) {
            FGAirport* airport = (FGAirport*)apts[apti].get();

            std::string airportId = airport->getId();
            boost::algorithm::to_upper(airportId);

            // add airport id name to the in-range list
            airportsInRange.push_back(airportId);

            m_AirportPosMap[airportId] = 
                SGGeod::fromDegFt(
                airport->getLongitude(), 
                airport->getLatitude(), 
                airport->getElevation());

        }

        // ensure airport lights are surface clamped
        MonitorAirportLighting(airportsInRange);

    }

    // --- clamp structures (static objects) ---

    // continually monitor static object clamping
    m_Objects->MonitorObjectClamping();
}


// reset surface light clamping
void OsgEarthSurfaceClamp::ResetClamping()
{
    // clear clamped airports map   
    m_ClampedAirportMap.clear();

    // clear static objects listener
    m_Objects->ResetClamping();
}



void OsgEarthSurfaceClamp::MonitorAirportLighting(
    const std::vector<std::string>& airportsInRange)
{
    {
        // --- continually monitor clamped airport's expiration ---

        std::vector<std::string> invalidateList;

        LightsClampMapType::iterator it;
        for (it = m_ClampedAirportMap.begin(); it != m_ClampedAirportMap.end(); ++it) {
            OsgEarthClampLightsVisitor* clv = it->second;
            if (clv->GetClampPhase() == OsgEarthClampLightsVisitor::IsClampedPhase) {
                if ((clv->GetTransformNode() == NULL) || 
                    (clv->GetSGOffsetTransformNode() == NULL)) 
                {
                    // entry not found, add airport to invalidate list
                    invalidateList.push_back(it->first);
                    SG_LOG(SG_TERRAIN, SG_INFO, std::endl 
                        << "[OsgEarthSurfaceClamp] "
                        "invalidating clamped airport: " << it->first);
                }
            }
        }

        for (unsigned int i = 0; i < invalidateList.size(); ++i) {
            m_ClampedAirportMap.erase(m_ClampedAirportMap.find(invalidateList[i]));
        }
    }

    // --- Clamp nearby airport lights as needed ---

    if (airportsInRange.size() > 0) {

        LightsClampMapType::iterator it;
        for (unsigned int i = 0; i < airportsInRange.size(); ++i) {
            it = m_ClampedAirportMap.find(airportsInRange[i]);

            if (it != m_ClampedAirportMap.end()) {

                // entry found

                if (it->second->GetClampPhase() == OsgEarthClampLightsVisitor::ReentrantPhase) {
                    // continue clamping lights (reentrant)
                    it->second->Process();
                }

            } else {

                // --- airport not found in list, lights need to be clamped ---

                SGGeod centerPos = globals->get_aircraft_position();

                // force a sample request 
                double h;
                osg::Vec3d n;
                simgear::OsgEarthHeightCache::Instance()->FindTerrainHeightInScene(centerPos, h, n);

                const float distanceToBeginClamping = 4000.0f;

                double distM = SGGeodesy::distanceM(m_AirportPosMap[airportsInRange[i]], centerPos);

                if ((distM <= distanceToBeginClamping) && 
                    simgear::OsgEarthHeightCache::Instance()->IsWithinTolerance())
                {

                    // airport is near enough, so clamp lights to surface

                    // target airport filename
                    std::string airportFilename = airportsInRange[i] + ".BTG";

                    // search terrain scene graph for matching airport
                    osgEarth::FindNodesVisitor<osg::Group> allGroupNodes;
                    globals->get_scenery()->get_terrain_branch()->accept(allGroupNodes);

                    std::vector<osg::Group*>::iterator ni;
                    for (ni = allGroupNodes._results.begin(); ni != allGroupNodes._results.end(); ++ni) {
                        osg::Group* grp = *ni;
                        std::string nameUpper = grp->getName();
                        boost::algorithm::to_upper(nameUpper);
                        if (nameUpper.find(airportFilename) != std::string::npos) {

                            osg::MatrixTransform* transformNode = 
                                dynamic_cast<osg::MatrixTransform*>(grp);

                            if (transformNode != NULL) {

                                // -- lights need to be clamped ---


                                osgEarth::FindNodesVisitor<SGOffsetTransform> offsetXformList;
                                transformNode->accept(offsetXformList);
                                if (offsetXformList._results.size() == 1) {
                                    SGOffsetTransform* sgXform = *offsetXformList._results.begin();

                                    // create the light clamp object
                                    osg::ref_ptr<OsgEarthClampLightsVisitor> clampIt = new OsgEarthClampLightsVisitor(
                                        airportsInRange[i], transformNode, sgXform);

                                    osg::Node* lightSubgraph = grp->getChild((unsigned int)(1));
                                    if ((lightSubgraph != NULL) && (lightSubgraph->getName() == "pagedObjectLOD")) {

                                        // process subgraph
                                        clampIt->Process(lightSubgraph);

                                        SG_LOG(SG_TERRAIN, SG_INFO, std::endl << 
                                            "[OsgEarthSurfaceClamp] Starting Light Clamping for [" 
                                            << airportsInRange[i] << "]" << std::endl << std::endl);

                                        m_ClampedAirportMap[airportsInRange[i]] = clampIt;
                                    }
                                } 
                            }

                            // airport found, stop looking
                            break;
                        }
                    }
                }
            }
        }
    }
}

#endif